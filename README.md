# A propos
Ce projet est le back de l'applicatif Gestion d'hotel. Il s'agit d'un exercice de réalisation de projet pour la fin de la formation developpeur full stack à GOMYCODE.


## Fonctionnalitées
- Gestion des utilisateur compte et connection. 
- Gestion des cocktails en CRUD
- Session utilisatuer gérée avec le JsonWebToken





# Installation
Une fois le dépôt cloné et une fois rendu dans le dossier du projet ne pas oublier d'installer les dépendances
``` 
npm install 
```

## Lancement

Ce projet démarre de 2 façon possible. Mode production et mode développement

```
npm run start
npm run dev
```