import React, { useEffect } from "react";
import "./user.css";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "../../../features/users/userSlice";

import {  Table } from "antd";

const User = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllUsers());
  }, []);

  const dataUser = useSelector((state) => state.user.users);


  const columns = [
    {
      title: "Nom",
      dataIndex: "nom",
      key: "id",
    },
    {
      title: "Prenom",
      dataIndex: "prenom",
      key: "id",
    },
    {
      title: "Pseudo",
      dataIndex: "pseudo",
      key: "id",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "id",
    },
    {
      title: "Mobile",
      dataIndex: "mobile",
      key: "id",
    },
    {
      title: "Statut",
      dataIndex: "statut",
      key: "id",
      render: (text, record) => {
        let className = ""; // Classe CSS par défaut

        // Vérifiez la valeur de "statut" et attribuez la classe CSS appropriée
        if (text === "supprimé") {
          className = "text-danger";
        } else if (text === "inactif") {
          className = "text-warning";
        } else if (text === "actif") {
          // Vous pouvez ajouter une classe différente pour "actif" ici si nécessaire
          className = "text-success";
        }

        // Renvoyez le texte avec la classe CSS
        return <span className={className}>{text}</span>;
      },
    },
  ];

  return (
    <div>
      <Table dataSource={dataUser} columns={columns} />
    </div>
  );
};

export default User;
