import React from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { createNewUser, resetUserState } from "../../../features/users/userSlice";
import { useDispatch } from "react-redux";
import { message } from "antd";


const AddUserSchema = Yup.object().shape({
  nom: Yup.string().required("Champ requis"),
  prenom: Yup.string().required("Champ requis"),
  pseudo: Yup.string().required("Champ requis"),
  email: Yup.string().required("Champ requis"),
  mobile: Yup.string().required("Champ requis"),
  password: Yup.string().required("Champ requis"),
  statut: Yup.string().required("Champ requis"),
});

const AddUser = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      nom: "",
      prenom: "",
      pseudo: "",
      mobile: "",
      email: "",
      password: "",
      statut: "",
    },
    validationSchema: AddUserSchema,
    onSubmit: async (values) => {
      const response = await dispatch(createNewUser(values));
      // Extrait le message de la réponse du serveur
      console.log(response.type);
      if (response.type == "user/createNewUser/fulfilled") {
        message.success("Chambre cree avec success");
        formik.resetForm();
        dispatch(resetUserState());
        navigate("/admin/list-user");
      } else if (response.type === "user/createNewUser/rejected") {
        console.log(response);
        message.error("Erreur lors de la création de l'utilisateur");
      } else {
        console.log(response);
        message.error("Erreur lors de la création de l'utilisateur");
      }
    },
  });
  return (
    <div className="">
      <h3 className="mb-5">
        {" "}
        <span className="text-decoration-underline">Ajouter</span> un
        utilisateur
      </h3>
      <form
        action=""
        onSubmit={formik.handleSubmit}
        className="container-fluid my-0-auto "
      >
        <div className="row">
          <div className=" col-md-6">
            <input
              type="text"
              className="form-control form-control-lg"
              id="nom"
              placeholder="Nom"
              name="nom"
              onChange={formik.handleChange} // Utilisez onChange
              value={formik.values.nom}
              onBlur={formik.handleBlur}
            />
            <div className="text-danger">
              {formik.touched.nom && formik.errors.nom ? (
                <div>{formik.errors.nom}</div>
              ) : null}
            </div>
          </div>
          <div className="col-md-6">
            <input
              type="text"
              className="form-control form-control-lg"
              id="prenom"
              placeholder="Prenom"
              name="prenom"
              onChange={formik.handleChange} // Utilisez onChange
              value={formik.values.prenom}
              onBlur={formik.handleBlur}
            />
            <div className="text-danger">
              {formik.touched.prenom && formik.errors.prenom ? (
                <div>{formik.errors.prenom}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-md-6">
            <input
              type="text"
              className="form-control form-control-lg"
              id="pseudo"
              placeholder="Pseudo"
              name="pseudo"
              onChange={formik.handleChange} // Utilisez onChange
              value={formik.values.pseudo}
              onBlur={formik.handleBlur}
            />
            <div className="text-danger">
              {formik.touched.pseudo && formik.errors.pseudo ? (
                <div>{formik.errors.pseudo}</div>
              ) : null}
            </div>
          </div>
          <div className="col-md-6">
            <input
              type="text"
              className="form-control form-control-lg"
              id="mobile"
              placeholder="Mobile"
              name="mobile"
              onChange={formik.handleChange} // Utilisez onChange
              value={formik.values.mobile}
              onBlur={formik.handleBlur}
            />
            <div className="text-danger">
              {formik.touched.mobile && formik.errors.mobile ? (
                <div>{formik.errors.mobile}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="mt-4">
          <input
            type="email"
            className="form-control form-control-lg"
            id="email"
            placeholder="Email"
            name="email"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.email && formik.errors.email ? (
              <div>{formik.errors.email}</div>
            ) : null}
          </div>
        </div>

        <div className="row mt-4">
          <div className="col-md-6">
            <input
              type="password"
              className="form-control form-control-lg"
              id="password"
              placeholder="Password"
              name="password"
              onChange={formik.handleChange} // Utilisez onChange
              value={formik.values.password}
              onBlur={formik.handleBlur}
            />
            <div className="text-danger">
              {formik.touched.password && formik.errors.password ? (
                <div>{formik.errors.password}</div>
              ) : null}
            </div>
          </div>
          <div className=" col-md-6">
            <select
              name="statut"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.statut}
              className="form-control py-2 mb-3"
              id=""
            >
              <option value="">select statut</option>
              <option value="actif">Actif</option>
              <option value="inactif">Inactif</option>
            </select>
            <div className="text-danger">
              {formik.touched.statut && formik.errors.statut ? (
                <div>{formik.errors.statut}</div>
              ) : null}
            </div>
          </div>
        </div>
        <button className="btn btn-outline-success btn-lg mt-2 float-end px-5">
          Ajouter
        </button>
      </form>
    </div>
  );
};

export default AddUser;
