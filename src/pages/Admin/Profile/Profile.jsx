import React from "react";
import { accountService } from "../../../services/accountService";
import { decodeToken } from "../../../utils/AxiosConfig/decodeToken ";
import "./profile.css"

const Profile = () => {
  const token = accountService.getToken(); // Remplacez par votre token JWT réel
  const userInfo = decodeToken(token);
  console.log(userInfo);
  return (
    <div className="border bg-white rounded-4 shadow py-5 px-2">
      <div>
        <h1 className="text-center text-decoration-underline title-profile">
          Informations Personnelles
        </h1>
        <ul className="list-unstyled text-start ms-2">
          <li className="fs-4 icon-profile ">Nom : {userInfo && userInfo.nom}</li>
          <li className="fs-4 icon-profile">Prenom : {userInfo && userInfo.prenom}</li>
          <li className="fs-4 icon-profile">Pseudo : {userInfo && userInfo.pseudo}</li>
          <li className="fs-4 icon-profile">Email : {userInfo && userInfo.email}</li>
          <li className="fs-4 icon-profile">Mobile : {userInfo && userInfo.mobile}</li>
        </ul>
        <div className="float-end">
          <span>Je dois encore gerer les 2 bouttons</span>
          <button className="btn btn-outline-warning me-4">
            Modifier mes infos
          </button>
          <button className="btn btn-outline-danger">
            Supprimer mon profile
          </button>
        </div>
      </div>
    </div>
  );
};

export default Profile;
