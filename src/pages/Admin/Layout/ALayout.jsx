import "./Alayout.css";
import React, { useState } from "react";
import {
  AiOutlinePicRight,
  AiOutlinePicLeft,
} from "react-icons/ai";

import { Layout, Button, theme } from "antd";
import { Outlet } from "react-router-dom";
import avatar from "../../../assets/images/users/avatar1.png";
import Asidebar from "../../../components/admin/Sidebar/Asidebar";
import { accountService } from "../../../services/accountService";
import { decodeToken } from "../../../utils/AxiosConfig/decodeToken ";
const { Header, Sider, Content } = Layout;



const ALayout = () => {
  const token = accountService.getToken() // Remplacez par votre token JWT réel
  const userInfo = decodeToken(token);

  // Utilisez les informations du token selon vos besoins
  if (userInfo) {
    console.log("Informations de l'utilisateur :", userInfo);
  } else {
    console.error("Impossible de décoder le token.");
  }
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          <h2 className="text-white fs-5 text-center py-3 mb-0">
            <span className="lg-logo">Latif Hotel</span>
            <span className="sm-logo">LH</span>
          </h2>
        </div>
        <div className="demo-logo-vertical" />
        <Asidebar />
      </Sider>
      <Layout>
        <Header
          className="d-flex justify-content-between ps-1 pe-5"
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          <Button
            type="text"
            icon={collapsed ? <AiOutlinePicRight /> : <AiOutlinePicLeft />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
          <div className="d-flex gap-2 align-items-center">
            

            <div className="d-flex gap-3 align-items-center">
              <div>
                <img src={avatar} alt=" user" width={30} height={30} />
              </div>
              <div className="d-flex flex-column justify-content-center">
                <h5 className="m-0 text-dark">
                 {`${userInfo.nom}  ${userInfo.prenom}`} 
                </h5>
                <p className="m-0 text-secondary lead">{userInfo.email}</p>
              </div>
            </div>
          </div>
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default ALayout;
