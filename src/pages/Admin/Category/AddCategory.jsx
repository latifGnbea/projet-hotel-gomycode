import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import {
  createNewCategory,
  getACategorie,
  resetCategoryState,
  updateCategory,
} from "../../../features/categories/categorySlice";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { message } from "antd";

const AddCategorySchema = Yup.object().shape({
  nom: Yup.string().required("Champ requis"),
});
const AddCategory = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {id} = useParams()
  const getCategoryId = id

  const newCategory = useSelector((state) => state.category);
  const { categories, isLoading, isError, isSuccess, data } = newCategory;

  useEffect(() => {
    if (getCategoryId !== undefined) {
      dispatch(getACategorie(getCategoryId));
    } else {
      dispatch(resetCategoryState());
    }
  }, [getCategoryId]);

  //  console.log(getTokenFromLocalStorage)
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      nom: data?.nom || "",
    },
    validationSchema: AddCategorySchema,
    onSubmit: async (values) => {
      if (getCategoryId !== undefined) {
        const data = { id: getCategoryId, dataCategory: values };
        dispatch(updateCategory(data)).then((res) => {
          if (res.type == "category/update-category/fulfilled") {
            message.success("Categorie modifie avec success");
            formik.resetForm();
            dispatch(resetCategoryState());
            navigate("/admin/category/list-category");
          } else if (res.error.message) {
            message.error(res.error.message);
          } else {
            message.error("Erreur lors de la création de la catégorie");
          }
        });
      } else {
        const response = await dispatch(createNewCategory(values));
        // Extrait le message de la réponse du serveur
        if (response.type == "category/createNewCategory/fulfilled") {
          message.success("Categorie cree avec success");
          formik.resetForm();
          dispatch(resetCategoryState());
          navigate("/admin/category/list-category");
        } else {
          console.log(response.response.data);
          message.error("Erreur lors de la création de la catégorie");
        }
      }
    },
  });
  return (
    <div>
      <h4> {getCategoryId ? "Modifier" : "Ajouter"} une categorie</h4>
      <form
        action=""
        onSubmit={formik.handleSubmit}
        className="container-fluid mx-0 "
      >
        <div className="mt-5">
          <input
            type="text"
            className="form-control form-control-lg"
            id="catg"
            placeholder="Nom de la categorie"
            name="nom"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.nom}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.nom && formik.errors.nom ? (
              <div>{formik.errors.nom}</div>
            ) : null}
          </div>
        </div>
        <button className="btn btn-outline-success btn-lg mt-2 float-end px-5">
          {getCategoryId ? "Modifier" : "Ajouter"}
        </button>
      </form>
    </div>
  );
};

export default AddCategory;
