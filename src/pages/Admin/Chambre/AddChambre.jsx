import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import "./chambre.css";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Select, message } from "antd";
import { resetServiceState } from "../../../features/service-chambre/serviceChambreSlice";
import {
  createNewChambre,
  getAChambre,
  resetChambreState,
  updateChambre,
} from "../../../features/chambres/chambreSlice";
import { getAllCategories } from "../../../features/categories/categorySlice";

const AddChambreSchema = Yup.object().shape({
  numero: Yup.string().required("Champ requis"),
  prix_nuit: Yup.string().required("Champ requis"),
  categorie: Yup.string().required("Champ requis"),
  disponibilite: Yup.string().required("Champ requis"),
});

const AddChambre = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const dataCategorie = useSelector((state) => state.category.categories);
  const newChambre = useSelector((state) => state.chambre);
  const { chambres, isLoading, isError, isSuccess, data } = newChambre;
  useEffect(() => {
    if (id !== undefined) {
      dispatch(getAChambre(id));
      dispatch(getAllCategories());
    } else {
      dispatch(getAllCategories());
      dispatch(resetServiceState());
    }
  }, [id]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      numero: data?.numero || "",
      categorie: data?.categorie || "",
      prix_nuit: data?.prix_nuit || "",
      disponibilite: data?.disponibilite || "",
    },
    validationSchema: AddChambreSchema,
    onSubmit: async (values) => {
      if (id !== undefined) {
        const data = { id: id, dataChambre: values };
        dispatch(updateChambre(data)).then((res) => {
          if (res.type == "chambre/update-chambre/fulfilled") {
            message.success("Chambre modifie avec success");
            formik.resetForm();
            dispatch(resetChambreState());
            navigate("/admin/chambre/list-chambre");
          } else if (res.error.message) {
            message.error(res.error.message);
          } else {
            message.error("Erreur lors de la modification de la chambre");
          }
        });
      } else {
        const response = await dispatch(createNewChambre(values));
        // Extrait le message de la réponse du serveur
       
        if (response.type == "chambre/createNewchambre/fulfilled") {
          message.success("Chambre cree avec success");
          formik.resetForm();
          dispatch(resetChambreState());
          navigate("/admin/chambre/list-chambre");
        } else if (response.type === "chambre/createNewchambre/rejected") {
            console.log(response);
          message.error("Erreur lors de la création de la chambre");
        } else {
          console.log(response);
          message.error("Erreur lors de la création de la chambre");
        }
      }
    },
  });

  return (
    <div>
      <h4> {id ? "Modifier" : "Ajouter"} une chambre</h4>
      <form
        action=""
        onSubmit={formik.handleSubmit}
        className="container-fluid mx-0 "
      >
        <div className="mt-5">
          <input
            type="text"
            className="form-control form-control-lg"
            id="chbre"
            placeholder="Nom de la chambre"
            name="numero"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.numero}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.numero && formik.errors.numero ? (
              <div>{formik.errors.numero}</div>
            ) : null}
          </div>
        </div>

        <div className="mt-5">
          <input
            type="number"
            className="form-control form-control-lg"
            id="prx"
            placeholder="Prix de la chambre par nuit"
            name="prix_nuit"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.prix_nuit}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.prix_nuit && formik.errors.prix_nuit ? (
              <div>{formik.errors.prix_nuit}</div>
            ) : null}
          </div>
        </div>

        <div className="row">
          <div className=" col-md-6 mt-5">
            <select
              name="categorie"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.categorie}
              className="form-control py-3 mb-3"
              id=""
            >
              <option value="" disabled>
                select categorie
              </option>
              {dataCategorie.map((item, index) => {
                return (
                  <option key={index} value={item.nom}>
                    {item.nom}
                  </option>
                );
              })}
            </select>
            <div className="text-danger">
              {formik.touched.categorie && formik.errors.categorie ? (
                <div>{formik.errors.categorie}</div>
              ) : null}
            </div>
          </div>

          <div className=" col-md-6 mt-5">
            <select
              name="disponibilite"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.disponibilite}
              className="form-control py-3 mb-3"
              id=""
            >
              <option value="default">select disponibilie</option>
              <option value="occupe">Occupe</option>
              <option value="disponible">Disponible</option>
              <option value="ferme">Ferme</option>
            </select>
            <div className="text-danger">
              {formik.touched.disponibilite && formik.errors.disponibilite ? (
                <div>{formik.errors.disponibilite}</div>
              ) : null}
            </div>
          </div>
        </div>

        <button className="btn btn-outline-success btn-lg mt-2 float-end px-5">
          {id ? "Modifier" : "Ajouter"}
        </button>
      </form>
    </div>
  );
};

export default AddChambre;
