import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import "./service.css";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  createNewService,
  getAService,
  resetServiceState,
  updateService,
} from "../../../features/service-chambre/serviceChambreSlice";
import { message } from "antd";

const AddServiceSchema = Yup.object().shape({
  nom: Yup.string().required("Champ requis"),
  description: Yup.string().required("Champ requis"),
  prix: Yup.string().required("Champ requis"),
});

const AddService = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const newCategory = useSelector((state) => state.service);
  const { categories, isLoading, isError, isSuccess, data } = newCategory;

  useEffect(() => {
    if (id !== undefined) {
      dispatch(getAService(id));
    } else {
      dispatch(resetServiceState());
    }
  }, [id]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      nom: data?.nom || "",
      description: data?.description || "",
      prix: data?.prix || "",
    },
    validationSchema: AddServiceSchema,
    onSubmit: async (values) => {
      if (id !== undefined) {
        const data = { id: id, dataService: values };
        dispatch(updateService(data)).then((res) => {
          if (res.type == "service/update-service/fulfilled") {
            message.success("Service modifie avec success");
            formik.resetForm();
            dispatch(resetServiceState());
            navigate("/admin/service/list-service");
          } else if (res.error.message) {
            message.error(res.error.message);
          } else {
            message.error("Erreur lors de la modification du service");
          }
        });
      } else {
        const response = await dispatch(createNewService(values));
        // Extrait le message de la réponse du serveur
        if (response.type == "service/createNewService/fulfilled") {
          message.success("Service cree avec success");
          formik.resetForm();
          dispatch(resetServiceState());
          navigate("/admin/service/list-service");
        } else {
          console.log(response.response.data);
          message.error("Erreur lors de la création du service");
        }
      }
    },
  });
  return (
    <div>
      <h4> {id ? "Modifier" : "Ajouter"} un service</h4>
      <form
        action=""
        onSubmit={formik.handleSubmit}
        className="container-fluid mx-0 "
      >
        <div className="mt-5">
          <input
            type="text"
            className="form-control form-control-lg"
            id="catg"
            placeholder="Nom du service"
            name="nom"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.nom}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.nom && formik.errors.nom ? (
              <div>{formik.errors.nom}</div>
            ) : null}
          </div>
        </div>
        <div className="mt-5">
          <input
            type="text"
            className="form-control form-control-lg"
            id="serv"
            placeholder="Description du service"
            name="description"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.description}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.description && formik.errors.description ? (
              <div>{formik.errors.description}</div>
            ) : null}
          </div>
        </div>
        <div className="mt-5">
          <input
            type="number"
            className="form-control form-control-lg"
            id="prx"
            placeholder="Prix du service"
            name="prix"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.prix}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger">
            {formik.touched.prix && formik.errors.prix ? (
              <div>{formik.errors.prix}</div>
            ) : null}
          </div>
        </div>
        <button className="btn btn-outline-success btn-lg mt-2 float-end px-5">
          {id ? "Modifier" : "Ajouter"}
        </button>
      </form>
    </div>
  );
};

export default AddService;
