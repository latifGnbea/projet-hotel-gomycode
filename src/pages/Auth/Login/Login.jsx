import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import "./login.css";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAsync } from "../../../features/auth/authSlice";

const loginSchema = Yup.object().shape({
  email: Yup.string().required("Champ requis"),
  password: Yup.string().required("Champ requis"),
});

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: "", // Renommez le champ "email"
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: async (values) => {
      // alert(JSON.stringify(values));
      dispatch(loginAsync(values));
      setTimeout(() => {
        navigate("/admin");
      }, 2000);
    },
  });

  return (
    <div className="bloc-form">
      
      <form id="form-login" onSubmit={formik.handleSubmit}>
      <h3 className="text-center mb-4">Page de connexion</h3>
        <div className="input-group mb-2">
          <input
            className="input-login"
            type="text"
            name="email"
            placeholder="Email Address"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.email}
            onBlur={formik.handleBlur} // Ajoutez onBlur si nécessaire
          />
          <div className="text-danger">
            {formik.touched.email && formik.errors.email ? (
              <div>{formik.errors.email}</div>
            ) : null}
          </div>
        </div>

        <div className="input-group mb-2">
          <input
            className="input-login"
            type="password"
            name="password"
            placeholder="Password"
            onChange={formik.handleChange} // Utilisez onChange
            value={formik.values.password}
            onBlur={formik.handleBlur} // Ajoutez onBlur si nécessaire
          />
          <div className="text-danger">
            {formik.touched.password && formik.errors.password ? (
              <div>{formik.errors.password}</div>
            ) : null}
          </div>
        </div>

        <button type="submit" className="">
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
