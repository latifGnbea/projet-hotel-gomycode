import React from "react";
import { Route, Routes } from "react-router-dom";
import Error from "../../utils/Error/Error";
import { Layout } from "../Public";
import Login from "./Login/Login";

const AuthRouter = () => {
  return (
    <Routes>
      <Route element={<Layout />}>
        <Route index element={<Login />} />
        <Route path="login" element={<Login />} />
        <Route path="*" element={<Error />} />
      </Route>
    </Routes>
  );
};

export default AuthRouter;
