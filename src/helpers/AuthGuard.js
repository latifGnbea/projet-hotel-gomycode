import { Navigate } from "react-router-dom";
import { accountService } from "../services/accountService";

const AuthGuard = ({ children }) => {
  if (!accountService.islogged()) {
    return <Navigate to="/auth/login" />;
  }
  return children;
};

export default AuthGuard;
