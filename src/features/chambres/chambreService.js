import axios from "axios";
import { base_url } from "../../utils/AxiosConfig/base_url";
import requestInterceptor from "../../utils/AxiosConfig/axiosConfig";

export const getAllChambre = async () => {
  try {
    const response = await axios.get(`${base_url}chambre/all`);
    // Vérifiez si la réponse a un code de statut HTTP 200 (OK)
    if (response.status == 200) {
      return response; // Les données de la catégorie sont renvoyées
    } else {
      throw new Error(
        "La requête a échoué avec un code de statut: " + response.status
      );
    }
  } catch (error) {
    // Renvoie une erreur avec un message descriptif
    throw new Error(
      "Une erreur s'est produite lors de la récupération des catégories: " +
        error.message
    );
  }
};

// creer
export const createChambre = async (chambre) => {
  try {
    const reponse = await axios.post(
      `${base_url}chambre/create`,
      chambre,
      requestInterceptor
    );
    return reponse;
  } catch (error) {
    throw new Error(error);
  }
};

// selectionner
export const getChambreById = async (id) => {
  try {
    const response = await axios.get(`${base_url}chambre/${id}`);
    return response;
  } catch (error) {
    // Si une erreur se produit, récupérez la réponse d'erreur du serveur si elle existe
    if (error.response) {
      // La réponse d'erreur du serveur se trouve dans error.response.data
      throw new Error("Server error: " + JSON.stringify(error.response.data));
    } else {
      // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
      throw new Error("An error occurred: " + error.message);
    }
  }
};

// modifier
const updateChambre = async (chambre) => {
  try {
    const response = await axios.patch(
      `${base_url}chambre/edit/${chambre.id}`,
      {
        numero: chambre.dataChambre.numero,
        prix_nuit: chambre.dataChambre.prix_nuit,
        categorie: chambre.dataChambre.categorie,
        disponibilite: chambre.dataChambre.disponibilite,
      }
    );
    return response;
  } catch (error) {
    if (error.response) {
      // La réponse d'erreur du serveur se trouve dans error.response.data
      throw new Error("Server error: " + JSON.stringify(error.response.data));
    } else {
      // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
      throw new Error("An error occurred: " + error.message);
    }
  }
};

//supprimer
const deleteChambre = async (id) => {
    try {
      const response = await axios.delete(`${base_url}chambre/delete/${id}`);
      return response;
    } catch (error) {
      if (error.response) {
        // La réponse d'erreur du serveur se trouve dans error.response.data
        throw new Error("Server error: " + JSON.stringify(error.response.data));
      } else {
        // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
        throw new Error("An error occurred: " + error.message);
      }
    }
  };

  export const chambreService = {
    getAllChambre,
    createChambre,
    getChambreById,
    updateChambre,
    deleteChambre
  }
