import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { chambreService } from "./chambreService";

export const getAllChambre = createAsyncThunk(
  "chambre/getAllChambre",
  async (_, { rejectWithValue }) => {
    try {
      const response = await chambreService.getAllChambre();
      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide : ", +response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const createNewChambre = createAsyncThunk(
  "chambre/createNewchambre",
  async (newChambre, { rejectWithValue }) => {
    try {
      // Appelez le service pour créer la catégorie
      const response = await chambreService.createChambre(newChambre);

      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide", +response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getAChambre = createAsyncThunk(
  "chambre/getAChambre",
  async (id, { rejectWithValue }) => {
    try {
      const response = await chambreService.getChambreById(id);
      // Retournez les données de la catégorie
      return response.data;
    } catch (error) {
      // Si une erreur se produit, lancez une exception avec le message d'erreur
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export const updateChambre = createAsyncThunk(
  "chambre/update-chambre",
  async (chambre) => {
    try {
      return await chambreService.updateChambre(chambre);
    } catch (error) {
      throw new Error(error);
    }
  }
);

// supprimer
export const deleteChambre = createAsyncThunk(
  "chambre/delete-chambre",
  async (id, thunkAPI) => {
    try {
      return await chambreService.deleteChambre(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

// Réinitialiser le state
export const resetChambreState = createAction("service/resetState");
const initialState = {
  chambres: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  errorMessage: "",
};

export const chambreSlice = createSlice({
  name: "chambre",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllChambre.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllChambre.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        state.chambres = action.payload;
        state.errorMessage = "";
      })
      .addCase(getAllChambre.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(createNewChambre.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createNewChambre.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        // Vous pouvez mettre à jour l'état avec la nouvelle catégorie ici si nécessaire.
        state.chambres.push(action.payload); // Par exemple, ajouter la nouvelle catégorie au tableau existant.
        state.errorMessage = "";
      })
      .addCase(createNewChambre.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(getAChambre.pending, (state) => {
        state.isLoading = true;
        state.isError = false;
        state.errorMessage = null;
      })
      .addCase(getAChambre.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.data = action.payload;
      })
      .addCase(getAChambre.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload; // Stocke le message d'erreur
      })
      .addCase(updateChambre.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateChambre.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.updatedChambre = action.payload;
      })
      .addCase(updateChambre.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.errorMessage = action.error;
      })
      .addCase(deleteChambre.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteChambre.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.deletedChambre = action.payload;
      })
      .addCase(deleteChambre.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetChambreState, (state) => {
        return initialState;
      });
  },
});

export default chambreSlice.reducer;
