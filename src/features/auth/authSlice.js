// authSlice.js
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { login } from "./authService";
import { accountService } from "../../services/accountService";

// Créez une fonction asynchrone pour effectuer la connexion
export const loginAsync = createAsyncThunk('auth/login', async (credentials) => {
  try {
    const response = await login(credentials);
    console.log(response);
    
    if (response.data && response.data.access_token) {
      accountService.saveToken(response.data.access_token)
      return response.data.access_token;
    } else {
      throw new Error('Invalid response format');
    }
  } catch (error) {
    console.log(error)
  }
});


// Créez un slice pour gérer l'état de l'authentification
const authSlice = createSlice({
  name: "auth",
  initialState: {
    token : null,
    user: null, // Les données de l'utilisateur connecté
    loading: false, // Indicateur de chargement pendant la connexion
    error: null, // Les erreurs d'authentification
  },
  reducers: {
    // Ajoutez des reducers supplémentaires ici si nécessaire
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginAsync.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(loginAsync.fulfilled, (state, action) => {
        state.loading = false;
        state.token = action.payload;
      })
      .addCase(loginAsync.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export default authSlice.reducer;
