import axios from "axios";
import { base_url } from "../../utils/AxiosConfig/base_url";
import requestInterceptor from "../../utils/AxiosConfig/axiosConfig";

// creer
export const createUser = async (user) => {
  try {
    const reponse = await axios.post(
      `${base_url}user/create`,
      user,
      requestInterceptor
    );
    return reponse;
  } catch (error) {
    throw new Error(error);
  }
};

export const getAllUsers = async () => {
  try {
    const response = await axios.get(`${base_url}user/all`, requestInterceptor);
    // Vérifiez si la réponse a un code de statut HTTP 200 (OK)
    if (response.status == 200) {
      return response; // Les données de la catégorie sont renvoyées
    } else {
      throw new Error(
        "La requête a échoué avec un code de statut: " + response
      );
    }
  } catch (error) {
    // Renvoie une erreur avec un message descriptif
    throw new Error(
      "Une erreur s'est produite lors de la récupération des catégories: " +
        error
    );
  }
};

// liste des utilisateurs
// const getAllUsers = async () => {
//   const response = await axios.get(`${base_url}user/all`);
//   return response.data;
// };

// selectionné une chambre
// const getaUser = async (id) => {
//   try {
//     const response = await axios.get(`${base_url}user/${id}`, config);
//     return response.data;
//   } catch (error) {
//     console.error(
//       "Une erreur s'est produite lors de la récupération de l'utilisateur:",
//       error
//     );
//     throw new Error(
//       "Une erreur s'est produite lors de la récupération de l'utilisateur."
//     );
//   }
// };

// modifier
// const updateUser = async (user) => {
//   try {
//     const response = await axios.put(
//       `${base_url}user/${user._id}`,
//       {
//         prenom: user.dataUser.prenom,
//         nom: user.dataUser.nom,
//         poste: user.dataUser.poste,
//         tel: user.dataUser.tel,
//         images: user.dataUser.images,
//         email: user.dataUser.email,
//         roles: user.dataUser.roles,
//       },
//       config
//     );
//     return response.data;
//   } catch (error) {
//     throw new Error("An error occurred while fetching users: " + error.message);
//   }
// };

// supprimer
// const deleteUser = async (id) => {
//   try {
//     const response = await axios.delete(`${base_url}user/${id}`, config);
//     return response.data;
//   } catch (error) {
//     throw new Error("An error occurred while fetching users: " + error.message);
//   }
// };

const userService = {
  getAllUsers,
  createUser,
  // getaUser,
  // updateUser,
  // deleteUser,
};

export default userService;
