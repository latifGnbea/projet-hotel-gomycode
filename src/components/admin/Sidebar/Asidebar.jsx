import React, { useState } from "react";
import "./Asidebar.css";
import { UserOutlined } from "@ant-design/icons";
import { BiCategory } from "react-icons/bi";
import { MdOutlineBedroomParent } from "react-icons/md";
import { AiOutlinePlusCircle, AiOutlineLike } from "react-icons/ai";
import {
  ProfileOutlined,
  DashboardOutlined,
  BarcodeOutlined,
  CarryOutOutlined,
  HomeOutlined,
  DisconnectOutlined
} from "@ant-design/icons";
import { Menu, Modal } from "antd";
import { useNavigate } from "react-router-dom";
import { accountService } from "../../../services/accountService";

const Asidebar = () => {
  const navigate = useNavigate();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const logout = () => {
    accountService.logout();
    navigate("/");
  };
  return (
    <div className="d-flex  justify-content-between flex-column" id="asidebar">
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={["/admin"]}
        onClick={({ key }) => {
          if (key === "signout") {
          } else {
            navigate(key);
            // console.log(key)
          }
        }}
        items={[
          {
            key: "/admin",
            icon: <DashboardOutlined className="fs-4" />,
            label: "Dashboard",
          },
          {
            key: "profile",
            icon: <ProfileOutlined className="fs-4" />,
            label: "Profile",
          },
          {
            key: "list-user",
            icon: <UserOutlined className="fs-4" />,
            label: "Utilisateur",
            children: [
              {
                key: "list-user", // Cette clé doit être unique
                icon: <UserOutlined className="fs-4" />,
                label: "la liste des utilisateurs",
              },
              {
                key: "add-user", // Cette clé doit être unique
                icon: <AiOutlinePlusCircle className="fs-4" />,
                label: "Ajouter un utilisateur",
              },
            ],
          },
          {
            key: "chambre",
            icon: <MdOutlineBedroomParent className="fs-4" />,
            label: "chambre",
            children: [
              {
                key: "chambre/list-chambre", // Cette clé doit être unique
                icon: <MdOutlineBedroomParent className="fs-4" />,
                label: "chambre list",
              },
              {
                key: "chambre/add-chambre", // Cette clé doit être unique
                icon: <AiOutlinePlusCircle className="fs-4" />,
                label: "Add chambre",
              },
            ],
          },
          {
            key: "categorie",
            icon: <BiCategory className="fs-4" />,
            label: "categorie",
            children: [
              {
                key: "category/list-category", // Cette clé doit être unique
                icon: <BiCategory className="fs-4" />,
                label: "categorie list",
              },
              {
                key: "category/add-category", // Cette clé doit être unique
                icon: <AiOutlinePlusCircle className="fs-4" />,
                label: "Add category",
              },
            ],
          },
          {
            key: "service",
            icon: <AiOutlineLike className="fs-4" />,
            label: "service",
            children: [
              {
                key: "service/list-service", // Cette clé doit être unique
                icon: <AiOutlineLike className="fs-4" />,
                label: "service list",
              },
              {
                key: "service/add-service", // Cette clé doit être unique
                icon: <AiOutlinePlusCircle className="fs-4" />,
                label: "Add service",
              },
            ],
          },
          {
            key: "reservation",
            icon: <BarcodeOutlined className="fs-4" />,
            label: "Les reservations",
          },
          {
            key: "facture",
            icon: <CarryOutOutlined className="fs-4" />,
            label: "Les factures",
          },
          {
            key: "/",
            icon: <HomeOutlined className="fs-4" />,
            label: "Home",
          },
        ]}
      />

      <div className="text-center">
        <button
          onClick={showModal}
          className="mt-5 mx-4 btn btn-danger lg-deco"
        >
         <DisconnectOutlined /> Deconnexion
        </button>
        <button
          onClick={showModal}
          className="mt-5 mx-4 btn btn-danger sm-deco"
        >
          <DisconnectOutlined />
        </button>
        <Modal
          title="Confirmation de déconnexion"
          open={isModalVisible}
          onOk={logout}
          onCancel={handleCancel}
        >
          <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
        </Modal>
      </div>
    </div>
  );
};

export default Asidebar;
